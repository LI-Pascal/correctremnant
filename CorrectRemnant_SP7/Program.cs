﻿using System;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Windows.Forms;             //  DialogResult
using Actcut.ActcutModelManager;        //  ActcutGeometryManager
using Alma.NetWrappers2D;               //  CutPartFactory
using Wpm.Implement.ComponentEditor;    //  IEntitySelector, EntitySelector
using Wpm.Implement.Manager;            //  IEntityList, IEntity, IModelRepository, IContext
using Wpm.Schema.Kernel;                //  ConditionOperator

namespace CorrectRemnant_SP7
{
    class Program
    {
        static string logFile = "";
        static void Main()
        {
            IContext Ctx = null;
            double epsilon = 0.01;
            string directory = "c:\\tempAlmacam\\";
            IModelsRepository modelsRepository = new ModelsRepository();
            IEntitySelector entitySelector = new EntitySelector
            {
                ShowPropertyBox = false,
                MultiSelect = false
            };
            entitySelector.Init(modelsRepository.Context, modelsRepository.Context.Kernel.CompositeEntityTypeList["MODEL"]);
            if (entitySelector.ShowDialog() == DialogResult.OK)
            {
                Console.Write("Connexion to database ");
                Ctx = modelsRepository.GetModelContext(entitySelector.SelectedEntity.FirstOrDefault().Id);
                Console.WriteLine(Ctx.DatabaseName);

                IEntityList sheetListOriginal = Ctx.EntityManager.GetEntityList("_SHEET", "_TYPE", ConditionOperator.Equal, 2);
                sheetListOriginal.Fill(false);
                logFile = directory + Ctx.DatabaseName + "-logFile.csv";

                if (!Directory.Exists(directory))
                    Directory.CreateDirectory(directory);
                else
                {
                    //  delete all files and folder
                    DirectoryInfo di = new DirectoryInfo(directory);
                    foreach (FileInfo file in di.GetFiles())
                        file.Delete();
                    foreach (DirectoryInfo dir in di.GetDirectories())
                        dir.Delete(true);
                }

                WriteLog("Start at " + DateTime.Now.ToString("yyyy/MM/dd-hh:mm:ss"));
                WriteLog("Database " + Ctx.DatabaseName);
                WriteLog();

                WriteLog(sheetListOriginal.Count() + " offcut to control");
                WriteLog();
                WriteLog("".PadRight(30, '-'));
                WriteLog();
                WriteLog("ID;NAME;X;Y;STATE");
                if (sheetListOriginal.Count > 0)
                {
                    var sheetList = sheetListOriginal.ToList();
                    IActcutGeometryManager actcutGeometryManager = ActcutGeometryManager.Create();

                    IEntityList geometryList = Ctx.EntityManager.GetEntityList("_GEOMETRY", "_NAME", ConditionOperator.Equal, "correctRemnant");
                    geometryList.Fill(false);

                    //  on supprime les géométrie portant le nom correctRemnant
                    foreach (IEntity geometry in geometryList)
                    {
                        geometry.Delete();
                    }

                    foreach (IEntity sheet in sheetList)
                    {
                        try
                        {
                            //  Export DPR files
                            string currentGeometry = directory + "remnant.dpr";
                            sheet.GetFieldValueInFile("_GEOMETRY", ref currentGeometry);

                            CutPartFactory.DprImporterPtr dprImporterPtr = CutPartFactory.DprImporter_New(currentGeometry);
                            CutPartFactory.DprImporter_Import(dprImporterPtr);
                            CutPartFactory.DprImporter_AssociateDprMachiningToCpfMachiningByDefault(dprImporterPtr);
                            CutPartFactory.PartFactoryPtr partFactoryPtr = CutPartFactory.DprImporter_NewPartFactory(dprImporterPtr);
                            CutPartFactory.GetFinalPartBoundingBox(partFactoryPtr, out double minX, out double minY, out double maxX, out double maxY);

                            //IEntity geometry = actcutGeometryManager.CreateGeometryFromDpr(Ctx, "correctRemnant", currentGeometry);

                            double width = maxY - minY;
                            double length = maxX - minX;
                            //double width = geometry.GetFieldValueAsDouble("_DIMENS2");
                            //double length = geometry.GetFieldValueAsDouble("_DIMENS1");
                            double widthRemnant = sheet.GetFieldValueAsDouble("_WIDTH");
                            double lengthRemnant = sheet.GetFieldValueAsDouble("_LENGTH");

                            if (Math.Abs(width - widthRemnant) > epsilon || Math.Abs(length - lengthRemnant) > epsilon)
                            {
                                //  sheet dimensions are bad
                                sheet.SetFieldValue("_WIDTH", width);
                                sheet.SetFieldValue("_LENGTH", length);
                                sheet.Save();
                                WriteLog(sheet.Id32 + ";" + sheet.DefaultValue + ";" + Math.Round(width, 2) + ";" + Math.Round(length, 2) + ";Modified");
                            }
                            else
                                WriteLog(sheet.Id32 + ";" + sheet.DefaultValue + ";" + Math.Round(width, 2) + ";" + Math.Round(length, 2) + ";Ok");

                            File.Delete(currentGeometry);
                            //geometry.Delete();
                        }
                        catch (Exception ex)
                        {
                            WriteLog(ex.Message);
                            WriteLog("Error on Remnant: " + sheet.Id32 + "|" + sheet.DefaultValue);
                        }
                    }
                }
                WriteLog();
                WriteLog("".PadRight(30, '-'));
                WriteLog();
                WriteLog("End at  " + DateTime.Now.ToString("yyyy/MM/dd-hh:mm:ss"));
                Process.Start(directory);
            }
        }

        static void WriteLog(string text = "")
        {
            using (StreamWriter sw = new StreamWriter(logFile, true))
            {
                Console.WriteLine(text);
                sw.WriteLine(text);
            }
        }
    }
}
